from pytrends.request import TrendReq
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


#specify host language, timezone, payload
#en-US is english, timezone is central standard(i.e. 360), google property filter (gprop) leave empty, category = 0: all categories relating to keyword

#get trend data
def get_searches(key_word, state):
    pytrends = TrendReq(hl='en-US', tz=360)
    pytrends.build_payload([key_word], cat=0, timeframe='2020-02-01 2020-02-10',  gprop='',geo=state)    
    df = pytrends.interest_over_time()
            
    print(df.head())
    
    sns.set()
    df['timestamp'] = pd.to_datetime(df.index)
    sns.lineplot(df['timestamp'], df[key_word],label=str(state))
    
    plt.title("Normalized Searches for {} in {}".format(key_word, state))
    plt.ylabel("Number of Searches")
    plt.xlabel("Date")
    plt.legend(loc='center right')

#https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
get_searches('Coronavirus', 'IS')
get_searches('Coronavirus', 'IE')
get_searches('Coronavirus', 'IM')
get_searches('Coronavirus', 'GB')
get_searches('Coronavirus', 'PT')
get_searches('Coronavirus', 'ES')
get_searches('Coronavirus', 'GI')


plt.show()